# MultiCurrencyTokenSale

Supposedly there's a token sale that sells token X for token Y. For ease of discussion, let's call token X (token for sale), SALE token and token Y (base currency) ETH.

This simple CLI app is to allow us to quickly determine the amount of SALE token

## Dependencies
* Node: 10.14.0

## Configuration

```npm install```

## Run the script

```npm start```
