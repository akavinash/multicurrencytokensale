// currencies to be fetched for live ratess
const currencies = ["bitcoin", "ethereum", "dogecoin"];

// mention currenciesSymbols in the same order as mentioned in input.txt file
const currenciesSymbols = ["BTC", "ETH", "DOGE"];

// currency to be used as a base currency
const saleCurrencySymbol = "ETH";

const coincapBaseUrl = "https://api.coincap.io/v2/assets"

module.exports = { currencies, currenciesSymbols, saleCurrencySymbol, coincapBaseUrl }
