const readline = require("readline");
const { readLine } = require("./lib/fileProcessing");

const rl = readline.createInterface({
  input: process.stdin,
});

async function initFileProcessing() {
  readLine(rl);
}

initFileProcessing();
