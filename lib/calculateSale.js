const BigNumber = require("bignumber.js");

/**
 * calculating the currency rates and returning the calculated rate
 *
 * @param {*} saleCurrencyRate
 * @param {*} currencyUsd
 * @param {*} baseSaleRate
 * @param {*} purchaseAmt
 * @param {*} decimalPlace
 */

module.exports.calculateSaleToken = async (
  saleCurrencyRate,
  currencyUsd,
  baseSaleRate,
  purchaseAmt,
  decimalPlace
) => {
  const currencyToBase = currencyUsd.dividedBy(saleCurrencyRate);

  const result = await currencyToBase
    .multipliedBy(baseSaleRate)
    .multipliedBy(purchaseAmt)
    .decimalPlaces(decimalPlace, BigNumber.ROUND_DOWN)
    .toFixed(decimalPlace)
    .toString();

  return result;
}
