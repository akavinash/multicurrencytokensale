const fetch = require("node-fetch");
const BigNumber = require("bignumber.js");
const { currencies, saleCurrencySymbol, coincapBaseUrl } = require('../../constants');

module.exports.fetchLiveRates = async () => {
  
  const response = await fetch(
    `${coincapBaseUrl}?ids=${currencies.join(",")}`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        "Connection": "keep-alive",
        "Content-Encoding": "gzip",
        "charset": "utf-8",
        "Date": Date.now(),
        "ETag": 'W/"7c43-Kis93RZINMxgTTQkQ1jLINrJXhU',
        "Transfer-Encoding": "chunked",
        "Vary": "Accept-Encoding",
        "X-Powered-By": "Express",
      },
    }
  );

  const payload = await response.json();
  const ratesObj = [];

  await payload.data.map((detail) => {
    const picked = (({ symbol, priceUsd }) => ({
      symbol,
      priceUsd: new BigNumber(priceUsd),
    }))(detail);
    
    if (detail.symbol === saleCurrencySymbol) {
      saleCurrencyRate = new BigNumber(detail.priceUsd);
    }
    ratesObj.push(picked);
  });
  
  return { ratesObj, saleCurrencyRate };
}
