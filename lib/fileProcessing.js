const { marketRates } = require("./marketRates");
const { processSaleDetails } = require("./processSale");

module.exports.readLine = async (rl) => {
  // rate of the base currency
  let saleCurrencyRate = undefined;

  // array for storing the rates of the mentioned currency
  let ratesObj = [];

  lockers = [];
  rl.on("line", async (line) => {
    lockers.push(
      // for synchronizing the code
      new Promise(async (resolve, reject) => {
        await Promise.all(lockers);
        
        if (ratesObj.length === 0) {
          const rates = await marketRates(line.split(" "));
          ratesObj = rates.ratesObj;
          saleCurrencyRate = rates.saleCurrencyRate;
        } else {
          result = await processSaleDetails(line, ratesObj, saleCurrencyRate);
          
          await printResult(result);
        }

        resolve();
      })
    );
  });
};

async function printResult(saleToken) {
  console.log(saleToken);
}
