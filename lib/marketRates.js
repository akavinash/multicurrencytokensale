const BigNumber = require("bignumber.js");
const { currenciesSymbols, saleCurrencySymbol } = require('../constants')
const { fetchLiveRates } =  require("./coincap");

/**
 * assigning the market rates
 *
 * @param {*} currenciesRate
 */

module.exports.marketRates = async (currenciesRate) => {
  const ratesObj = [];
  let saleCurrencyRate = 0;

  if (currenciesSymbols.includes(currenciesRate[2])) {
    console.error(
      "rates for the currencies are not provided in index.txt file"
    );
  }

  if (currenciesRate[0].toLowerCase() === "current") {
    try {
      // fetching the market rate by calling API
      return await fetchLiveRates();
    } catch (err) {
      console.log("err : ", err);
    }
  } else {
    // fetching the rate from the file
    saleCurrencyRate = currenciesRate[currenciesSymbols.indexOf(saleCurrencySymbol)];
    
    for (index in currenciesSymbols) {
      ratesObj.push({
        symbol: currenciesSymbols[index],
        priceUsd: new BigNumber(currenciesRate[index]),
      });
    }

    return { ratesObj, saleCurrencyRate };
  }
};
