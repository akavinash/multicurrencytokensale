const { calculateSaleToken } = require("./calculateSale");

/**
 * calling the function to calculate the dale token and printing the response
 *
 * @param {*} splittedLine
 * @param {*} ratesObj
 */

module.exports.processSaleDetails = async (
  saleDetails,
  ratesObj,
  saleCurrencyRate
) => {
  if (!saleCurrencyRate) {
    return console.error("rates for the currencies are not provided");
  }

  [baseSaleRate, decimalPlace, purchaseCurrency, purchaseAmt] = saleDetails.split(" ");
  const rateObj = ratesObj.filter(rateObj => { return rateObj.symbol === purchaseCurrency });

  return await calculateSaleToken(
    saleCurrencyRate,
    rateObj[0].priceUsd,
    baseSaleRate,
    purchaseAmt,
    parseInt(decimalPlace)
  );
};
